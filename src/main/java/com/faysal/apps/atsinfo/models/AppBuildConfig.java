package com.faysal.apps.atsinfo.models;

public final class AppBuildConfig {
    public static final String APPLICATION_ID = "com.faysal.apps.atsinfo";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 6;
    public static final String VERSION_NAME = "1.4";
}
