package com.faysal.apps.atsinfo.models

import android.graphics.drawable.Drawable


/*
* Created by Faysal Ahmed Shakil
* Software Engineer @APPBD
*/

class ATSInfo(var flags: Int, var appLogo: Drawable, var appLable: String, var packageName: String)
