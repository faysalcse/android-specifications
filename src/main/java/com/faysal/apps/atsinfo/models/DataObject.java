package com.faysal.apps.atsinfo.models;

public class DataObject {
    private String mText1;
    private String mText2;

    public DataObject(String text1, String text2) {
        this.mText1 = text1;
        this.mText2 = text2;
    }

    public String getmText1() {
        return this.mText1;
    }

    public void setmText1(String mText1) {
        this.mText1 = mText1;
    }

    public String getmText2() {
        return this.mText2;
    }

    public void setmText2(String mText2) {
        this.mText2 = mText2;
    }
}
