package com.faysal.apps.atsinfo.utilities;

import android.content.Context;
import android.hardware.Camera;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CameraOperations {
    Context context;

    public CameraOperations(Context context) {
        this.context = context;
    }

    public int backCameraMp(){
        Camera camera=Camera.open(0);    // For Back Camera
        android.hardware.Camera.Parameters params = camera.getParameters();
        List sizes = params.getSupportedPictureSizes();
        Camera.Size  result = null;

        int mega_pixel = 0;

        ArrayList<Integer> arrayListForWidth = new ArrayList<Integer>();
        ArrayList<Integer> arrayListForHeight = new ArrayList<Integer>();

        for (int i=0;i<sizes.size();i++){
            result = (Camera.Size) sizes.get(i);
            arrayListForWidth.add(result.width);
            arrayListForHeight.add(result.height);
        }
        if(arrayListForWidth.size() != 0 && arrayListForHeight.size() != 0){
            System.out.println("Back max W :"+ Collections.max(arrayListForWidth));              // Gives Maximum Width
            System.out.println("Back max H :"+Collections.max(arrayListForHeight));                 // Gives Maximum Height
            System.out.println("Back Megapixel :"+( ((Collections.max(arrayListForWidth)) * (Collections.max(arrayListForHeight))) / 1024000 ) );

            mega_pixel=((((Collections.max(arrayListForWidth)) * (Collections.max(arrayListForHeight))) / 1024000 ));
        }
        camera.release();

        arrayListForWidth.clear();
        arrayListForHeight.clear();

        return mega_pixel;

    }


    public int fontCameraMp(){
        Camera camera=Camera.open(1);        //  For Front Camera
        android.hardware.Camera.Parameters params1 = camera.getParameters();
        List sizes1 = params1.getSupportedPictureSizes();

        int megaPixel=0;

        ArrayList<Integer> arrayListForWidth = new ArrayList<Integer>();
        ArrayList<Integer> arrayListForHeight = new ArrayList<Integer>();


        Camera.Size  result1 = null;
        for (int i=0;i<sizes1.size();i++){
            result1 = (Camera.Size) sizes1.get(i);
            arrayListForWidth.add(result1.width);
            arrayListForHeight.add(result1.height);
        }
        if(arrayListForWidth.size() != 0 && arrayListForHeight.size() != 0){
            System.out.println("FRONT max W :"+Collections.max(arrayListForWidth));
            System.out.println("FRONT max H :"+Collections.max(arrayListForHeight));
            System.out.println("FRONT Megapixel :"+( ((Collections.max(arrayListForWidth)) * (Collections.max(arrayListForHeight))) / 1024000 ) );

            megaPixel=(((Collections.max(arrayListForWidth)) * (Collections.max(arrayListForHeight))) / 1024000);
        }

        camera.release();

        return megaPixel;
    }


     public String backCameraResolution(){
        Camera camera=Camera.open(0);    // For Back Camera
        android.hardware.Camera.Parameters params = camera.getParameters();
        List sizes = params.getSupportedPictureSizes();
        Camera.Size  result = null;

        String resolution = null;

        ArrayList<Integer> arrayListForWidth = new ArrayList<Integer>();
        ArrayList<Integer> arrayListForHeight = new ArrayList<Integer>();

        for (int i=0;i<sizes.size();i++){
            result = (Camera.Size) sizes.get(i);
            arrayListForWidth.add(result.width);
            arrayListForHeight.add(result.height);
        }
        if(arrayListForWidth.size() != 0 && arrayListForHeight.size() != 0){
            System.out.println("Back max W :"+ Collections.max(arrayListForWidth));              // Gives Maximum Width
            System.out.println("Back max H :"+Collections.max(arrayListForHeight));                 // Gives Maximum Height
            resolution=(Collections.max(arrayListForWidth))+" x "+Collections.max(arrayListForHeight);
        }
        camera.release();

        arrayListForWidth.clear();
        arrayListForHeight.clear();

        return resolution;

    }


    public String fontCameraResolution(){
        Camera camera=Camera.open(1);        //  For Front Camera
        android.hardware.Camera.Parameters params1 = camera.getParameters();
        List sizes1 = params1.getSupportedPictureSizes();

        String resolution = null;

        ArrayList<Integer> arrayListForWidth = new ArrayList<Integer>();
        ArrayList<Integer> arrayListForHeight = new ArrayList<Integer>();


        Camera.Size  result1 = null;
        for (int i=0;i<sizes1.size();i++){
            result1 = (Camera.Size) sizes1.get(i);
            arrayListForWidth.add(result1.width);
            arrayListForHeight.add(result1.height);
        }
        if(arrayListForWidth.size() != 0 && arrayListForHeight.size() != 0){
            System.out.println("FRONT max W :"+Collections.max(arrayListForWidth));
            System.out.println("FRONT max H :"+Collections.max(arrayListForHeight));
            System.out.println("FRONT Megapixel :"+( ((Collections.max(arrayListForWidth)) * (Collections.max(arrayListForHeight))) / 1024000 ) );

            resolution = (Collections.max(arrayListForWidth))+" x "+Collections.max(arrayListForHeight);
        }

        camera.release();
        return resolution;
    }

    public int getBackCameraResolutionInMp()
    {
        int noOfCameras = Camera.getNumberOfCameras();
        float maxResolution = -1;
        long pixelCount = -1;
        for (int i = 0;i < noOfCameras;i++)
        {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            Camera.getCameraInfo(i, cameraInfo);

            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK)
            {
                Camera camera = Camera.open(i);;
                Camera.Parameters cameraParams = camera.getParameters();
                for (int j = 0;j < cameraParams.getSupportedPictureSizes().size();j++)
                {
                    long pixelCountTemp = cameraParams.getSupportedPictureSizes().get(j).width * cameraParams.getSupportedPictureSizes().get(j).height; // Just changed i to j in this loop
                    if (pixelCountTemp > pixelCount)
                    {
                        pixelCount = pixelCountTemp;
                        maxResolution = ((float)pixelCountTemp) / (1024000.0f);
                    }
                }

                camera.release();
            }
        }

        int mx= (int)Math.round(maxResolution);

        return mx;
    }


    public int getFontCameraResolutionInMp()
    {
        int noOfCameras = Camera.getNumberOfCameras();
        float maxResolution = -1;
        long pixelCount = -1;
        for (int i = 0;i < noOfCameras;i++)
        {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            Camera.getCameraInfo(i, cameraInfo);

            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)
            {
                Camera camera = Camera.open(i);;
                Camera.Parameters cameraParams = camera.getParameters();
                for (int j = 0;j < cameraParams.getSupportedPictureSizes().size();j++)
                {
                    long pixelCountTemp = cameraParams.getSupportedPictureSizes().get(j).width * cameraParams.getSupportedPictureSizes().get(j).height; // Just changed i to j in this loop
                    if (pixelCountTemp > pixelCount)
                    {
                        pixelCount = pixelCountTemp;
                        maxResolution = ((float)pixelCountTemp) / (1024000.0f);
                    }
                }

                camera.release();
            }
        }

        int mx= (int)Math.round(maxResolution);

        return mx;
    }



}
