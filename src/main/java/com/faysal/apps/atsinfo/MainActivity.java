package com.faysal.apps.atsinfo;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.faysal.apps.atsinfo.fragments.AppsFragment;
import com.faysal.apps.atsinfo.fragments.BatteryFragment;
import com.faysal.apps.atsinfo.fragments.BlueToothFragment;
import com.faysal.apps.atsinfo.fragments.CPUFragment;
import com.faysal.apps.atsinfo.fragments.CameraFragment;
import com.faysal.apps.atsinfo.fragments.DRMFragment;
import com.faysal.apps.atsinfo.fragments.DisplayFragment;
import com.faysal.apps.atsinfo.fragments.GPUFragment;
import com.faysal.apps.atsinfo.fragments.HomeFragment;
import com.faysal.apps.atsinfo.fragments.NetworkFragment;
import com.faysal.apps.atsinfo.fragments.OSFragment;
import com.faysal.apps.atsinfo.fragments.PhoneFeaturesFragment;
import com.faysal.apps.atsinfo.fragments.SensorCategoryFragment;
import com.faysal.apps.atsinfo.fragments.SimFragment;
import com.faysal.apps.atsinfo.fragments.StorageFragment;
import com.faysal.apps.atsinfo.helper.SharedHelper;
import com.faysal.apps.atsinfo.models.ATSInfo;
import com.faysal.apps.atsinfo.utilities.FragmentUtil;
import com.faysal.apps.atsinfo.utilities.KeyUtil;
import com.faysal.apps.atsinfo.utilities.Methods;
import com.faysal.apps.atsinfo.utilities.RateUsApp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/*
* Created by Faysal Ahmed Shakil
* Software Engineer @APPBD
*/


public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    public BottomSheetBehavior mBottomSheetBehavior;
    private TextView toolbarText;
    private TextView botomBarText;
    ImageView menuAction;
    LinearLayout toolbarLayout;

    Switch swEnableDarkTheme;




    private CardView category_home,category_os,category_sensor,category_cpu,
            category_battery,category_network,category_sim,category_camera,category_storage,
            category_bluetooth,category_display,category_features,category_userapps,
            category_systemapps,category_developer_info,category_gpu,category_rateus,category_drminfo;
    ImageView shareBtn;



    private Handler mHandler = new Handler();

    View bottomSheet;
    public FragmentUtil fragmentUtil;

    ProgressDialog dialog;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;
        if (SharedHelper.getKey(this,"dark").equals("true")){
            setTheme(R.style.AppThemeDark);
        }
        setContentView(R.layout.activity_main_an);


        swEnableDarkTheme = findViewById(R.id.swEnableDarkTheme);
        if (SharedHelper.getKey(this,"dark").equals("true")){
            swEnableDarkTheme.setChecked(true);
        }else {
            swEnableDarkTheme.setChecked(false);
        }

        swEnableDarkTheme.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                    SharedHelper.putKey(context,"dark","true");
                    resartActivity();
                }else {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                    SharedHelper.putKey(context,"dark","false");
                    resartActivity();
                }
            }
        });



        bottomSheet = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);

        toolbarLayout = findViewById(R.id.toolbarLayout);


        toolbarText = findViewById(R.id.toolbarText);
        botomBarText = findViewById(R.id.botomBarText);
        menuAction= findViewById(R.id.menuAction);
        shareBtn= findViewById(R.id.shareBtn);
        category_home=(CardView)findViewById(R.id.category_home);
        category_os=(CardView)findViewById(R.id.category_os);
        category_sensor=(CardView)findViewById(R.id.category_sensor);
        category_cpu=(CardView)findViewById(R.id.category_cpu);
        category_battery=(CardView)findViewById(R.id.category_battery);
        category_network=(CardView)findViewById(R.id.category_network);
        category_sim=(CardView)findViewById(R.id.category_sim);
        category_camera=(CardView)findViewById(R.id.category_camera);
        category_storage=(CardView)findViewById(R.id.category_storage);
        category_bluetooth=(CardView)findViewById(R.id.category_bluetooth);
        category_display=(CardView)findViewById(R.id.category_display);
        category_features=(CardView)findViewById(R.id.category_features);
        category_userapps=(CardView)findViewById(R.id.category_userapps);
        category_systemapps=(CardView)findViewById(R.id.category_systemapps);
        category_developer_info=(CardView)findViewById(R.id.category_developer_info);
        category_gpu=(CardView)findViewById(R.id.category_gpu);
        category_rateus=(CardView)findViewById(R.id.category_rateus);
        category_drminfo=(CardView)findViewById(R.id.category_drminfo);

        category_home.setOnClickListener(this);
        category_os.setOnClickListener(this);
        category_sensor.setOnClickListener(this);
        category_cpu.setOnClickListener(this);
        category_battery.setOnClickListener(this);
        category_network.setOnClickListener(this);
        category_sim.setOnClickListener(this);
        category_camera.setOnClickListener(this);
        category_storage.setOnClickListener(this);
        category_bluetooth.setOnClickListener(this);
        category_display.setOnClickListener(this);
        category_features.setOnClickListener(this);
        category_userapps.setOnClickListener(this);
        category_systemapps.setOnClickListener(this);
        category_home.setOnClickListener(this);
        category_developer_info.setOnClickListener(this);
        category_gpu.setOnClickListener(this);
        shareBtn.setOnClickListener(this);
        category_rateus.setOnClickListener(this);
        category_drminfo.setOnClickListener(this);

       // setFragment(new HomeFragment());




        fragmentUtil = new FragmentUtil(MainActivity.this);
        fragmentUtil.clearBackStackFragmets();
        fragmentUtil.replaceFragment(new HomeFragment(), false, true);



        menuAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               botomSheetStateAction();

            }
        });

        botomBarText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                botomSheetStateAction();
            }
        });



        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    switch (newState) {
                        case BottomSheetBehavior.STATE_COLLAPSED:
                            menuAction.setImageResource(R.drawable.ic_menu_vector);
                            break;
                        case BottomSheetBehavior.STATE_DRAGGING:

                            break;
                        case BottomSheetBehavior.STATE_EXPANDED:
                            menuAction.setImageResource(R.drawable.ic_down_arrow);

                            break;
                        case BottomSheetBehavior.STATE_HIDDEN:

                            break;
                        case BottomSheetBehavior.STATE_SETTLING:

                            break;
                    }
                }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
               /* toolbarText.setText("Sliding...");*/
            }
        });






   }

    private void resartActivity() {
        Intent intent=new Intent(this,MainActivity.class);
        startActivity(intent);
        finish();
    }


    public void hideToolbarBotomSheet(){
            bottomSheet.setVisibility(View.GONE);
            toolbarLayout.setVisibility(View.GONE);
   }


    public void expandeToolbarBotomSheet(){
        bottomSheet.setVisibility(View.VISIBLE);
        toolbarLayout.setVisibility(View.VISIBLE);
    }
    public void showProgressDialog(String msg){
        dialog=new ProgressDialog(MainActivity.this);
        dialog.setMessage(msg);
        dialog.setCancelable(false);
        dialog.show();

    }

    public void dismissDialog(){
        dialog.dismiss();
    }




    public void botomSheetStateAction(){
        if (mBottomSheetBehavior.getState()== BottomSheetBehavior.STATE_COLLAPSED){
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }
        if (mBottomSheetBehavior.getState()== BottomSheetBehavior.STATE_EXPANDED){
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    public void setToolbarName(String name,int color){
        toolbarText.setText(name);
        toolbarText.setTextColor(color);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.category_home:
                loadHomeFragment(0);
                return;
            case R.id.category_os:
                loadHomeFragment(1);
                return;
            case R.id.category_sensor:
                loadHomeFragment(2);
                return;
            case R.id.category_cpu:
                loadHomeFragment(3);
                return;
            case R.id.category_battery:
                setToolbarName(getResources().getString(R.string.toolbar_battery),getResources().getColor(R.color.dark_green));
                loadHomeFragment(4);
                botomSheetStateAction();
                return;
            case R.id.category_network:
                setToolbarName(getResources().getString(R.string.toolbar_network),getResources().getColor(R.color.dark_sky_blue));
                loadHomeFragment(5);
                botomSheetStateAction();
                return;
            case R.id.category_sim:
                /*setToolbarName(getResources().getString(R.string.toolbar_sim),getResources().getColor(R.color.dark_parrot_green));
                loadHomeFragment(6);*/
                Toast.makeText(this, getResources().getString(R.string.uder_development), Toast.LENGTH_SHORT).show();
                botomSheetStateAction();
                return;
            case R.id.category_camera:
                showProgressDialog("Please wait...");
                loadHomeFragment(7);
                return;
            case R.id.category_storage:
                setToolbarName(getResources().getString(R.string.toolbar_storage),getResources().getColor(R.color.dark_red));
                loadHomeFragment(8);
                botomSheetStateAction();
                return;
            case R.id.category_bluetooth:
                setToolbarName(getResources().getString(R.string.toolbar_bluetooth),getResources().getColor(R.color.dark_blue_one));
                loadHomeFragment(9);
                botomSheetStateAction();
                return;
            case R.id.category_display:
                setToolbarName(getResources().getString(R.string.toolbar_dispalay),getResources().getColor(R.color.dark_violet_one));
                loadHomeFragment(10);
                botomSheetStateAction();
                return;
            case R.id.category_features:
                setToolbarName(getResources().getString(R.string.toolbar_features),getResources().getColor(R.color.dark_brown));
                loadHomeFragment(11);
                botomSheetStateAction();
                return;
            case R.id.category_userapps:
                showProgressDialog("Please wait");
                //Toast.makeText(this, "Please Wait", Toast.LENGTH_SHORT).show();
                loadHomeFragment(12);
                return;
            case R.id.category_systemapps:
                showProgressDialog("Please wait");
                //Toast.makeText(this, "Please Wait", Toast.LENGTH_SHORT).show();
                loadHomeFragment(13);
                return;
            case R.id.category_drminfo:
                showProgressDialog("Please wait...");
                loadHomeFragment(14);
                botomSheetStateAction();
                return;
            case R.id.category_developer_info:
                //loadHomeFragment(14);
                botomSheetStateAction();
                startActivity(new Intent(this,InfoActivity.class));
                return;
            case R.id.category_rateus:
                RateUsApp.Companion.rateUsApp(MainActivity.this);
                botomSheetStateAction();
                launchMarket();
                return;
            case R.id.shareBtn:
                botomSheetStateAction();
                Methods.sharing("https://play.google.com/store/apps/details?id="+getPackageName());
                return;
            case R.id.category_gpu:
                loadHomeFragment(20);
                setToolbarName(getResources().getString(R.string.toolbar_gpu),getResources().getColor(R.color.colorPrimaryDark));
                return;
            default:
                setToolbarName(getResources().getString(R.string.toolbar_home),getResources().getColor(R.color.colorPrimaryDark));
                return;
        }
    }

        private void launchMarket() {
            Uri uri = Uri.parse("market://details?id=" + getPackageName());
            Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
            try {
                startActivity(myAppLinkToMarket);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(this, " unable to find market app", Toast.LENGTH_LONG).show();
            }
        }



    private void selectNavMenu() {
       // navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }


    public void loadHomeFragment(final int pos) {
        // selecting appropriate nav menu item
        selectNavMenu();

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Fragment fragment = getFragmentFromDrawer(pos);
                if (fragment != null) {
                    fragmentUtil.clearBackStackFragmets();
                    fragmentUtil.replaceFragment(fragment, false, true);
                }
            }
        }, 300);

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    /**
     * Get the fragment while need to push.
     *
     * @return: fragment
     */
    private Fragment getFragmentFromDrawer(int navItemIndex) {
        switch (navItemIndex) {
            case 0:
                return HomeFragment.getInstance(0);
            case 1:
                return new OSFragment();
            case 2:
                return new SensorCategoryFragment();
            case 3:
                return new CPUFragment();
            case 4:
                return new BatteryFragment();
            case 5:
                return new NetworkFragment();
            case 6:
                return new SimFragment();
            case 7:
                return new CameraFragment();
            case 8:
                return new StorageFragment();
            case 9:
                return new BlueToothFragment();
            case 10:
                return new DisplayFragment();
            case 11:
                return new PhoneFeaturesFragment();
            case 12:
                return AppsFragment.Companion.getInstance(KeyUtil.IS_USER_COME_FROM_USER_APPS);
            case 13:
                return AppsFragment.Companion.getInstance(KeyUtil.IS_USER_COME_FROM_SYSTEM_APPS);
            case 14:
                return new DRMFragment();
            case 15:

                break;
            case 16:
               // RateUsApp.Companion.rateUsApp(MainActivity.this);
                break;
            case 17:
                return HomeFragment.getInstance(7);
            case 18:
                return HomeFragment.getInstance(7);
            case 19:
                return HomeFragment.getInstance(7);
            case 20:
                return GPUFragment.getInstance(0);
            default:
                return new HomeFragment();
        }
        return null;
    }

    public List<ATSInfo> getAppsList() {
        List<ATSInfo> ATSInfos = new ArrayList<>();

        int flags = PackageManager.GET_META_DATA | PackageManager.GET_SHARED_LIBRARY_FILES;

        PackageManager pm = getPackageManager();
        List<ApplicationInfo> applications = pm.getInstalledApplications(flags);

        for (ApplicationInfo appInfo : applications) {
            if ((appInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 1) {
                // System application
                Drawable icon = pm.getApplicationIcon(appInfo);
                ATSInfos.add(new ATSInfo(1, icon, pm.getApplicationLabel(appInfo).toString(), appInfo.packageName));
            } else {
                // Installed by User
                Drawable icon = pm.getApplicationIcon(appInfo);
                ATSInfos.add(new ATSInfo(2, icon, pm.getApplicationLabel(appInfo).toString(), appInfo.packageName));
            }
        }
        return ATSInfos;
    }


}