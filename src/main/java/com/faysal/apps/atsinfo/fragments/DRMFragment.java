package com.faysal.apps.atsinfo.fragments;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.drm.DrmInfo;
import android.drm.DrmInfoRequest;
import android.drm.DrmManagerClient;
import android.hardware.camera2.CameraCharacteristics;
import android.media.MediaDrm;
import android.media.UnsupportedSchemeException;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.faysal.apps.atsinfo.R;
import com.faysal.apps.atsinfo.models.AppBuildConfig;
import com.faysal.apps.atsinfo.models.DataObject;
import com.faysal.apps.atsinfo.utilities.Methods;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static org.apache.commons.lang3.StringUtils.capitalize;


/*
 * Created by Faysal Ahmed Shakil
 * Software Engineer @APPBD
 */
public class DRMFragment extends BaseFragment {



    private static final long DEVICE_IS_NOT_PROVISIONED = 1;
    private static final long DEVICE_IS_PROVISIONED = 0;
    private static final long DEVICE_IS_PROVISIONED_SD_ONLY = 2;

    Unbinder unbinder;

    @BindView(R.id.tv_api_level)
    TextView tvApiLevel;
    @BindView(R.id.tv_brand_name)
    TextView tvBrand;
    @BindView(R.id.tv_model_number)
    TextView tvModel;
    @BindView(R.id.tv_hardware)
    TextView tvHardware;
    @BindView(R.id.tv_manufacturer)
    TextView tvManufacturer;
    @BindView(R.id.tv_fingerprint)
    TextView tvFingerprint;
    @BindView(R.id.tv_drm_status)
    TextView tvDrmStatus;
    @BindView(R.id.tv_drm_vendor)
    TextView tvDrmVendor;
    @BindView(R.id.tv_drm_version)
    TextView tvDrmVersion;
    @BindView(R.id.tv_drm_algorithm)
    TextView tvDrmAlgorithm;
    @BindView(R.id.tv_drm_max_number_of_open_sessions)
    TextView tvDrmMaxNumberOpenSessions;
    @BindView(R.id.tv_drm_system_id)
    TextView tvDrmSystemId;
    @BindView(R.id.tv_drm_security_level)
    TextView tvDrmSecurityLevel;
    @BindView(R.id.tv_drm_max_hdcp_level_supported)
    TextView tvDrmMaxHdcpLevel;
    @BindView(R.id.tv_drm_max_number_of_sessions)
    TextView tvDrmMaxNumberSessions;
    @BindView(R.id.tv_drm_usage_reporting)
    TextView tvDrmUsageReporting;
    @BindView(R.id.tv_drm_version_name)
    TextView tvDRMVersionName;
    @BindView(R.id.tv_support_plugin)
    TextView tvSupportPlugin;



    private static final String WIDEVINE_MIME_TYPE = "video/wvm";
    private static final UUID WIDEVINE_UUID = new UUID(-1301668207276963122L, -6645017420763422227L);
    private static String algorithms;
    private static String description;
    private static String hdcpLevel;
    private static String maxHdcpLevel;
    private static String maxNumberOfSessions;
    private static String numberOfOpenSessions;
    private static String securityLevel;
    private static String systemId;
    private static String usageReportingSupport;
    private static String vendor;
    private static String version;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final Context contextThemeWrapper = new ContextThemeWrapper(getActivity(), R.style.OSTheme);
        LayoutInflater localInflater = inflater.cloneInContext(contextThemeWrapper);
        View view = localInflater.inflate(R.layout.fragment_drm, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.drm_info));
            window.setNavigationBarColor(getResources().getColor(R.color.drm_info));

        }


        return view;
    }



    public static int fetchPrimaryDarkColor(Resources.Theme context) {
        TypedValue typedValue = new TypedValue();
        TypedArray a = context.obtainStyledAttributes(typedValue.data, new int[]{R.attr.colorPrimaryDark});
        int color = a.getColor(0, 0);
        a.recycle();
        return color;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity.dismissDialog();
        mActivity.setToolbarName(getResources().getString(R.string.toolbar_drm),getResources().getColor(R.color.drm_info));
        mActivity.botomSheetStateAction();
        getOSInfo();
        setDRMinformations();
    }



    private void getOSInfo() {

       // tvApiLevel.setText("".concat(String.valueOf(Build.VERSION.SDK_INT)));

        tvBrand.setText("".concat(capitalize(Build.BRAND)));
        tvModel.setText("".concat(getDeviceName()));
        tvManufacturer.setText("".concat(StringUtils.capitalize(Build.MANUFACTURER)));
        tvFingerprint.setText("".concat(Build.FINGERPRINT));
        tvHardware.setText("".concat(StringUtils.capitalize(Build.HARDWARE)));

        int CVersion = Build.VERSION.SDK_INT;
        switch (CVersion) {
            case 11:
                tvApiLevel.setText(mResources.getString(R.string.honeycomb).concat("-".concat(String.valueOf(Build.VERSION.SDK_INT))));
                break;

            case 12:
                tvApiLevel.setText(mResources.getString(R.string.honeycomb).concat("-".concat(String.valueOf(Build.VERSION.SDK_INT))));
                break;

            case 13:
                tvApiLevel.setText(mResources.getString(R.string.honeycomb).concat("-".concat(String.valueOf(Build.VERSION.SDK_INT))));
                break;

            case 14:
                tvApiLevel.setText(mResources.getString(R.string.ics).concat("-".concat(String.valueOf(Build.VERSION.SDK_INT))));
                break;

            case 15:
                tvApiLevel.setText(mResources.getString(R.string.ics).concat("-".concat(String.valueOf(Build.VERSION.SDK_INT))));
                break;

            case 16:
                tvApiLevel.setText(mResources.getString(R.string.jellybean).concat("-".concat(String.valueOf(Build.VERSION.SDK_INT))));
                break;

            case 17:
                tvApiLevel.setText(mResources.getString(R.string.jellybean).concat("-".concat(String.valueOf(Build.VERSION.SDK_INT))));
                break;

            case 18:
                tvApiLevel.setText(mResources.getString(R.string.jellybean).concat("-".concat(String.valueOf(Build.VERSION.SDK_INT))));
                break;

            case 19:
                tvApiLevel.setText(mResources.getString(R.string.kitkat).concat("-".concat(String.valueOf(Build.VERSION.SDK_INT))));
                break;

            case 21:
                tvApiLevel.setText(mResources.getString(R.string.lollipop).concat("-".concat(String.valueOf(Build.VERSION.SDK_INT))));
                break;

            case 22:
                tvApiLevel.setText(mResources.getString(R.string.lollipop).concat("-".concat(String.valueOf(Build.VERSION.SDK_INT))));
                break;

            case 23:
                tvApiLevel.setText(mResources.getString(R.string.marshmallow).concat("-".concat(String.valueOf(Build.VERSION.SDK_INT))));
                break;

            case 24:
                tvApiLevel.setText(mResources.getString(R.string.nougat).concat("-".concat(String.valueOf(Build.VERSION.SDK_INT))));
                break;

            case 25:
                tvApiLevel.setText(mResources.getString(R.string.nougat).concat("-".concat(String.valueOf(Build.VERSION.SDK_INT))));
                break;
            case 26:
                tvApiLevel.setText(mResources.getString(R.string.oreo).concat("-".concat(String.valueOf(Build.VERSION.SDK_INT))));
                break;
            case 27:
                tvApiLevel.setText(mResources.getString(R.string.oreo).concat("-".concat(String.valueOf(Build.VERSION.SDK_INT))));
                break;
            case 28:
                tvApiLevel.setText(mResources.getString(R.string.pie).concat("-".concat(String.valueOf(Build.VERSION.SDK_INT))));
                break;
            default:
                tvApiLevel.setText(mResources.getString(R.string.unknown_version));
                break;
        }



    }

    private void setDRMinformations(){
        getWVDrmInfo();
        tvDrmVendor.setText("".concat(vendor));
        tvDRMVersionName.setText("".concat(description));
        tvDrmVersion.setText("".concat(version));
        tvDrmAlgorithm.setText("".concat(algorithms));
        tvDrmSystemId.setText("".concat(systemId));
        tvDrmSecurityLevel.setText("".concat(securityLevel));
        tvDrmMaxHdcpLevel.setText("".concat(hdcpLevel));
        tvDrmMaxNumberSessions.setText("".concat(maxNumberOfSessions));
        tvDrmMaxNumberOpenSessions.setText("".concat(numberOfOpenSessions));
        tvDrmUsageReporting.setText("".concat(usageReportingSupport));
        tvDrmStatus.setText("".concat(isDeviceWidevineDRMProvisioned(mActivity) ? "Supported" : "Not Supported"));

        tvSupportPlugin.setText("".concat(AppBuildConfig.FLAVOR + getDrmSupportedPlugins()));
    }

    private ArrayList<DataObject> getDataSet() {
        ArrayList results = new ArrayList();
        for (int index = 0; index < 5; index++) {
            if (index == 2) {
                results.add(index, new DataObject("Supported DRM Plugin's : \n", AppBuildConfig.FLAVOR + getDrmSupportedPlugins()));
            } else if (index == 0) {
                String myDeviceModel = Build.MODEL;
                String myDevice = Build.DEVICE;
                String myBoard = Build.BOARD;
                results.add(index, new DataObject("Device Information : \n", "Model     : " + myDeviceModel + "\nDevice    : " + myDevice + "\nBrand     : " + Build.BRAND + "\nSDK Level : " + Build.VERSION.SDK_INT));
            } else if (index == 1) {
                getWVDrmInfo();
                results.add(index, new DataObject("DRM Information : \n", "Status : ( ".concat(isDeviceWidevineDRMProvisioned(mActivity) ? "Supported" : "Not Supported").toUpperCase() + " )\n" + "Vendor : ( ".concat(vendor).toUpperCase() + " )\n" + "Version : ( ".concat(version).toUpperCase() + " )\n" + "Description : ( ".concat(description).toUpperCase() + " )\n" + "Security Level : ( ".concat(securityLevel).toUpperCase() + " )\n" + "HDCP Level : ( ".concat(hdcpLevel).toUpperCase() + " )\n" + "Max HDCP Level :( ".concat(maxHdcpLevel).toUpperCase() + " )\n" + "Max No. of Sessions : ( ".concat(maxNumberOfSessions).toUpperCase() + " )\n" + "No. of Open Sessions : ( ".concat(numberOfOpenSessions).toUpperCase() + " )\n" + "Usage Reporting : ( ".concat(usageReportingSupport).toUpperCase() + " )\n"));
            }
        }
        return results;
    }

    private String getDrmSupportedPlugins() {
        String text = AppBuildConfig.FLAVOR;
        DrmManagerClient drmManagerClient = new DrmManagerClient(mActivity);
        try {
            DrmManagerClient.class.getMethod("loadPlugIns", new Class[0]).invoke(drmManagerClient, new Object[0]);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e2) {
            e2.printStackTrace();
        } catch (IllegalArgumentException e3) {
            e3.printStackTrace();
        } catch (IllegalAccessException e4) {
            e4.printStackTrace();
        } catch (InvocationTargetException e5) {
            e5.printStackTrace();
        }
        for (String engine : drmManagerClient.getAvailableDrmEngines()) {
            text = text + engine + "\n";
            Log.i("VerifyDrmPluginsSupport", engine);
        }
        return text;
    }

    public static boolean isDeviceWidevineDRMProvisioned(Context context) {
        if (Build.VERSION.SDK_INT < 19) {
            return false;
        }
        DrmManagerClient drmManagerClient = new DrmManagerClient(context);
        DrmInfoRequest drmInfoRequest = new DrmInfoRequest(1, WIDEVINE_MIME_TYPE);
        drmInfoRequest.put("WVPortalKey", "key provided for drm in widevine portal");
        DrmInfo drmInfo = drmManagerClient.acquireDrmInfo(drmInfoRequest);
        if (drmInfo == null) {
            return true;
        }
        String kWVDrmInfoRequestStatusKey = (String) drmInfo.get("WVDrmInfoRequestStatusKey");
        String drmPath = (String) drmInfo.get("drm_path");
        if ((kWVDrmInfoRequestStatusKey == null || ((long) Integer.parseInt(kWVDrmInfoRequestStatusKey)) != DEVICE_IS_NOT_PROVISIONED) && (drmPath == null || drmPath.length() != 0)) {
            return true;
        }
        return false;
    }


    /** Returns the consumer friendly device name */
    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    // Assumes every value type has a reasonable toString()
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private String formatCameraCharacteristics(CameraCharacteristics info) {
        String infoText;
        if (info != null) {
            StringBuilder infoBuilder = new StringBuilder(
                    "Camera characteristics:\n\n");

            for (CameraCharacteristics.Key<?> key : info.getKeys()) {
                infoBuilder.append(String.format(Locale.US, "%s:  ",
                        key.getName()));

                Object val = info.get(key);
                if (val.getClass().isArray()) {
                    // Iterate an array-type value
                    // Assumes camera characteristics won't have arrays of arrays as values
                    int len = Array.getLength(val);
                    infoBuilder.append("[ ");
                    for (int i = 0; i < len; i++) {
                        infoBuilder.append(String.format(Locale.US, "%s%s",
                                Array.get(val, i), (i + 1 == len) ? ""
                                        : ", "));
                    }
                    infoBuilder.append(" ]\n\n");
                } else {
                    // Single value
                    infoBuilder.append(String.format(Locale.US, "%s\n\n",
                            val.toString()));
                }
            }
            infoText = infoBuilder.toString();
        } else {
            infoText = "No info";
        }
        return infoText;
    }


    @SuppressLint("WrongConstant")
    @TargetApi(18)
    private void getWVDrmInfo() {
        UnsupportedSchemeException e;
        try {
            MediaDrm mediaDrm = new MediaDrm(WIDEVINE_UUID);
            MediaDrm mediaDrm2;
            try {
                vendor = mediaDrm.getPropertyString("vendor");
                version = mediaDrm.getPropertyString("version");
                description = mediaDrm.getPropertyString("description");
                algorithms = mediaDrm.getPropertyString("algorithms");
                securityLevel = mediaDrm.getPropertyString("securityLevel");
                systemId = mediaDrm.getPropertyString("systemId");
                hdcpLevel = mediaDrm.getPropertyString("hdcpLevel");
                maxHdcpLevel = mediaDrm.getPropertyString("maxHdcpLevel");
                usageReportingSupport = mediaDrm.getPropertyString("usageReportingSupport");
                maxNumberOfSessions = mediaDrm.getPropertyString("maxNumberOfSessions");
                numberOfOpenSessions = mediaDrm.getPropertyString("numberOfOpenSessions");
                mediaDrm.release();
            } catch (Exception e2) {
                e = (UnsupportedSchemeException) e2;
                e.printStackTrace();
            }
        } catch (UnsupportedSchemeException e3) {
            e = e3;
            e.printStackTrace();
        }
    }
}
