package com.faysal.apps.atsinfo.fragments;


import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.faysal.apps.atsinfo.R;
import com.faysal.apps.atsinfo.utilities.KeyUtil;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/*
 * Created by Faysal Ahmed Shakil
 * Software Engineer @APPBD
 */
public class GPUFragment extends BaseFragment {

    Unbinder unbinder;
    int mode;
    @BindView(R.id.tv_resolution)
    TextView tv_resolution;
    @BindView(R.id.tv_graphics_device_name)
    TextView tv_graphics_device_name;
    @BindView(R.id.tv_graphics_device_vendor)
    TextView tv_graphics_device_vendor;
    @BindView(R.id.tv_graphics_device_version)
    TextView tv_graphics_device_version;
    @BindView(R.id.tv_graphics_device_extensions)
    TextView tv_graphics_device_extensions;
    @BindView(R.id.tv_max_texture_size)
    TextView tv_max_texture_size;

    LinearLayout rootLayout;

    private String mVendor,renderer,version,extension,texture;
    private GLSurfaceView mGlSurfaceView;
    public static final String TAG="GPU_FS";

    private GLSurfaceView glSurfaceView;

    private GLSurfaceView.Renderer mGlRenderer;


    public static GPUFragment getInstance(int mode) {
        GPUFragment homeFragment = new GPUFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(KeyUtil.KEY_MODE, mode);
        homeFragment.setArguments(bundle);

        return homeFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_gpu, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.fragment_gpu));
            window.setNavigationBarColor(getResources().getColor(R.color.fragment_gpu));

        }

         mActivity.setToolbarName(getActivity().getResources().getString(R.string.toolbar_gpu), getActivity().getResources().getColor(R.color.fragment_gpu));
         mActivity.botomSheetStateAction();


        rootLayout=view.findViewById(R.id.rootLayout);

        mGlRenderer = new GLSurfaceView.Renderer() {

            @Override
            public void onSurfaceCreated(GL10 gl, EGLConfig config) {// TODO Auto-generated method stub
                Log.d(TAG, "gl renderer: "+gl.glGetString(GL10.GL_RENDERER));
                Log.d(TAG, "gl vendor: "+gl.glGetString(GL10.GL_VENDOR));
                Log.d(TAG, "gl version: "+gl.glGetString(GL10.GL_VERSION));
                Log.d(TAG, "gl extensions: "+gl.glGetString(GL10.GL_EXTENSIONS));

               // tv_graphics_device_name.setText(gl.glGetString(GL10.GL_RENDERER));
            /*tv_graphics_device_vendor.setText("".concat(gl.glGetString(GL10.GL_VENDOR)));
            tv_graphics_device_version.setText("".concat(gl.glGetString(GL10.GL_VERSION)));
            tv_graphics_device_extensions.setText("".concat(gl.glGetString(GL10.GL_EXTENSIONS)));*/
                // tv_max_texture_size.setText("".concat(gl.glGetString(GL10.GL_MAX_TEXTURE_SIZE)));

                 mVendor = gl.glGetString(GL10.GL_VENDOR);
                 renderer = gl.glGetString(GL10.GL_RENDERER);
                 version = gl.glGetString(GL10.GL_VERSION);
                 extension = gl.glGetString(GL10.GL_EXTENSIONS);
                 texture = gl.glGetString(GL10.GL_MAX_TEXTURE_SIZE);

            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    tv_graphics_device_name.setText(renderer);
                    tv_graphics_device_vendor.setText(mVendor);
                    tv_graphics_device_version.setText(version);
                    tv_graphics_device_extensions.setText(extension);
                    tv_max_texture_size.setText(texture);
                    rootLayout.removeView(mGlSurfaceView);

                }
            });
            }

            @Override
            public void onSurfaceChanged(GL10 gl, int width, int height) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onDrawFrame(GL10 gl) {
                // TODO Auto-generated method stub

            }
        };

        mGlSurfaceView = new GLSurfaceView(getActivity());
        mGlSurfaceView.setRenderer(mGlRenderer);
        rootLayout.addView(mGlSurfaceView);



        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getBundleData();
        getDeviceInfo();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);

    }


    private void getDeviceInfo() {

        WindowManager wm = (WindowManager) mActivity.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;

        tv_resolution.setText("".concat(width + " * " + height + " " + "Pixels"));

    }

    /**
     * Get data from bundle
     */
    private void getBundleData() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(KeyUtil.KEY_MODE)) {
                mode = bundle.getInt(KeyUtil.KEY_MODE);
            }
        }
    }

}
