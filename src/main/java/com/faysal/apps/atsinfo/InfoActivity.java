package com.faysal.apps.atsinfo;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class InfoActivity extends AppCompatActivity {

    TextView devName;
    TextView devMail;

    Button rateUs;
    Button shareBtn;
    Button updateBtn;
    Button moreApp;
    Button contactMe;


    ImageView homeAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_about_us);

        devName=(TextView)findViewById(R.id.devName);
        devMail=(TextView)findViewById(R.id.devMail);

        devName.setText("Faysal Ahmed Shakil");
        devMail.setText("fsfoysal15@gmail.com");

        rateUs=(Button) findViewById(R.id.rateBtn);
        shareBtn=(Button)findViewById(R.id.shareBtn);
        updateBtn=(Button)findViewById(R.id.updateBtn);
        moreApp=(Button)findViewById(R.id.moreAppBtn);
        contactMe=(Button)findViewById(R.id.contactMe);
        homeAction=(ImageView) findViewById(R.id.homeAction);

        homeAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        rateUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id="+getApplicationContext().getPackageName()));
                startActivity(intent);
            }
        });


        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url="https://play.google.com/store/apps/details?id="+getApplicationContext().getPackageName();
                Intent localIntent = new Intent();
                localIntent.setAction("android.intent.action.SEND");
                localIntent.putExtra("android.intent.extra.TEXT", url);
                localIntent.setType("text/plain");
                startActivity(Intent.createChooser(localIntent, "Chose one"));

            }
        });


        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id="+getApplicationContext().getPackageName()));
                startActivity(intent);
            }
        });

        contactMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("https://www.facebook.com/fsfaysalcse"));
                startActivity(intent);
            }
        });


        moreApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getOpenFacebookIntent(getApplicationContext());

            }
        });


    }

    public static Intent getOpenFacebookIntent(Context context) {

        try {
            context.getPackageManager()
                    .getPackageInfo("com.facebook.katana", 0); //Checks if FB is even installed.
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("fb://profile/100006089748385")); //Trys to make intent with FB's URI
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.facebook.com/fsfaysalcse")); //catches and opens a url to the desired page
        }
    }
}
