package com.faysal.apps.atsinfo.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.faysal.apps.atsinfo.MainActivity
import com.faysal.apps.atsinfo.R
import com.faysal.apps.atsinfo.models.FeaturesHW

/*
* Created by Faysal Ahmed Shakil
* Software Engineer @APPBD
*/


class CPUAdapter(internal var appslist: ArrayList<FeaturesHW>, internal var mActivity: MainActivity) : RecyclerView.Adapter<CPUAdapter.DeviceVH>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): DeviceVH {
        val itemView = LayoutInflater.from(mActivity).inflate(R.layout.row_cpu_item, p0, false)
        return DeviceVH(itemView)
    }

    override fun onBindViewHolder(p0: DeviceVH, p1: Int) {
        p0?.bindData(appslist[p1], p1)
    }



    override fun getItemCount(): Int = appslist.size

    inner class DeviceVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(featureHW: FeaturesHW, position: Int) {

            val tvFeatureName: TextView = itemView.findViewById(R.id.tv_cpu_feature_name)
            val tvFeatureValue: TextView = itemView.findViewById(R.id.tv_cpu_feature_value)
            val viewSeparator: View = itemView.findViewById(R.id.separatorView)

            tvFeatureName.text = featureHW.featureLable
            tvFeatureValue.text = featureHW.featureValue

            if (featureHW.featureLable == mActivity.resources.getString(R.string.processor) && position > 0) {
                viewSeparator.visibility = View.VISIBLE
            } else {
                viewSeparator.visibility = View.GONE
            }
        }
    }
}