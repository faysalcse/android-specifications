package com.faysal.apps.atsinfo.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.faysal.apps.atsinfo.MainActivity
import com.faysal.apps.atsinfo.R
import com.faysal.apps.atsinfo.models.FeaturesHW


/*
* Created by Faysal Ahmed Shakil
* Software Engineer @APPBD
*/

class CameraAdapter(internal var appslist: ArrayList<FeaturesHW>, internal var mActivity: MainActivity) : RecyclerView.Adapter<CameraAdapter.DeviceVH>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): DeviceVH {
        val itemView = LayoutInflater.from(mActivity).inflate(R.layout.row_camera_item, p0, false)
        return DeviceVH(itemView)
    }

    override fun onBindViewHolder(p0: DeviceVH, p1: Int) {
        p0?.bindData(appslist[p1])
        println("appList = " + appslist.size)
    }


    override fun getItemCount(): Int = appslist.size


    inner class DeviceVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(featureHW: FeaturesHW) {

            val tvFeatureName: TextView = itemView.findViewById(R.id.tv_camera_feature_name)
            val tvFeatureValue: TextView = itemView.findViewById(R.id.tv_camera_feature_value)

            tvFeatureName.text = featureHW.featureLable
            tvFeatureValue.text = featureHW.featureValue
        }
    }
}