package com.faysal.apps.atsinfo.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.faysal.apps.atsinfo.MainActivity
import com.faysal.apps.atsinfo.R
import com.faysal.apps.atsinfo.models.SimInfo
import com.faysal.apps.atsinfo.utilities.Methods

/*
* Created by Faysal Ahmed Shakil
* Software Engineer @APPBD
*/

class SimTwoAdapter(private val mainActivity: MainActivity, private var simInformationData: ArrayList<SimInfo>): RecyclerView.Adapter<SimTwoAdapter.SimVH>(){

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): SimVH {
        val itemView = LayoutInflater.from(mainActivity).inflate(R.layout.row_sim_item, p0, false)
        return SimVH(itemView)
    }

    override fun onBindViewHolder(p0: SimVH, p1: Int) {
        p0?.bindData(simInformationData[p1])
    }


    override fun getItemCount(): Int = simInformationData.size



    inner class SimVH(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bindData(simInfo: SimInfo) {

            val tvLabel: TextView? = itemView.findViewById(R.id.tvLabel)
            val tvSimInformation: TextView? = itemView.findViewById(R.id.tvSimInformation)

            tvLabel?.text = simInfo.simLable
            tvSimInformation?.text = simInfo.simData

            itemView.setOnClickListener({
                Methods.avoidDoubleClicks(itemView)
            })
        }
    }
}