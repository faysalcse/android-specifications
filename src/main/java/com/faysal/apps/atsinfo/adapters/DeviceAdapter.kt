package com.faysal.apps.atsinfo.adapters

import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.faysal.apps.atsinfo.MainActivity
import com.faysal.apps.atsinfo.R
import com.faysal.apps.atsinfo.models.ATSInfo
import com.faysal.apps.atsinfo.utilities.Methods


/*
* Created by Faysal Ahmed Shakil
* Software Engineer @APPBD
*/


class DeviceAdapter(internal var flag: Int?, internal var appslist: ArrayList<ATSInfo>, internal var mActivity: MainActivity) : RecyclerView.Adapter<DeviceAdapter.DeviceVH>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): DeviceVH {
        val itemView = LayoutInflater.from(mActivity).inflate(R.layout.row_infomation, p0, false)
        return DeviceVH(itemView)
    }

    override fun onBindViewHolder(p0: DeviceVH, p1: Int) {
        p0?.bindData(appslist[p1])
    }


    override fun getItemCount(): Int {
        return appslist.size
    }

    inner class DeviceVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(ATSInfo: ATSInfo) {

            val ivAppLogo: ImageView? = itemView.findViewById(R.id.iv_app_icon)
            val tvAppname: TextView? = itemView.findViewById(R.id.tv_app_name)
            val tvAppPackageName: TextView? = itemView.findViewById(R.id.tv_app_package_name)

            tvAppname?.text = ATSInfo.appLable
            tvAppPackageName?.text = ATSInfo.packageName
            ivAppLogo?.setImageDrawable(ATSInfo.appLogo)

            itemView.setOnClickListener({
                Methods.avoidDoubleClicks(itemView)

                val launchIntent = mActivity.packageManager.getLaunchIntentForPackage(ATSInfo.packageName)
                if (launchIntent != null) {
                    startActivity(mActivity, launchIntent, null)
                }
            })
        }
    }
}
